using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace MassGraph.DAL
{
    public class GraphContext : DbContext
    {
        // Your context has been configured to use a 'GraphModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MassGraph.DAL.GraphModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'GraphModel' 
        // connection string in the application configuration file.

        #region Constructors

        public GraphContext()
            : base("name=GraphContext")
        {
            Database.SetInitializer<GraphContext>(new DropCreateDatabaseIfModelChanges<GraphContext>());
            this.Configuration.LazyLoadingEnabled = false;
        }

        #endregion

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        #region Properties

        public virtual DbSet<Edge> Edges { get; set; }

        public virtual DbSet<Node> Nodes { get; set; }

        #endregion
    }

    public class Node
    {
        #region Properties

        public virtual ICollection<Edge> Edges { get; set; }
        public string Name { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NodeId { get; set; }

        #endregion
    }

    public class Edge
    {
        #region Properties

        [Key]
        public int EdgeId { get; set; }

        [ForeignKey("Node1Id")]
        [InverseProperty("Edges")]
        public virtual Node Node1 { get; set; }

        public int? Node1Id { get; set; }

        [ForeignKey("Node2Id")]
        public virtual Node Node2 { get; set; }

        public int? Node2Id { get; set; }

        #endregion
    }
}