﻿using Microsoft.Owin;

using Owin;

[assembly: OwinStartup(typeof(MassGraph.Web.Service.Startup))]

namespace MassGraph.Web.Service
{
    public class Startup
    {
        #region Public Methods

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
        }

        #endregion
    }
}