﻿using System.Collections.Generic;

using MassGraph.Business.Common.Dto;
using MassGraph.Business.Common.Interfaces;

namespace MassGraph.Web.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataManagementService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DataManagementService.svc or DataManagementService.svc.cs at the Solution Explorer and start debugging.
    public class DataManagementService : IDataManagementService
    {
        #region Properties

        public IGraphEngine GraphEngine { private get; set; }

        #endregion

        #region IDataManagementService Members

        public void ImportAllNodes(IList<NodeDto> nodes)
        {
            GraphEngine.ImportAllNodes(nodes);
        }

        public void WipeAllData()
        {
            GraphEngine.WipeAllData();
        }

        #endregion
    }
}