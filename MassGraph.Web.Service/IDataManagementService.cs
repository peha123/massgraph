﻿using System.Collections.Generic;
using System.ServiceModel;

using MassGraph.Business.Common.Dto;

namespace MassGraph.Web.Service
{
    [ServiceContract]
    public interface IDataManagementService
    {
        #region Public Methods

        [OperationContract]
        void ImportAllNodes(IList<NodeDto> nodes);

        [OperationContract]
        void WipeAllData();

        #endregion
    }
}