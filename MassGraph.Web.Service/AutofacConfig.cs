﻿using Autofac;
using Autofac.Configuration;

using Bootstrap.Autofac;

namespace MassGraph.Web.Service
{
    /// <summary>
    /// Autofac modules configuration.
    /// </summary>
    public class AutofacConfig : IAutofacRegistration
    {
        #region IAutofacRegistration Members

        /// <summary>
        /// Register Autofac modules.
        /// </summary>
        /// <param name="builder"></param>
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterModule(new ConfigurationSettingsReader("autofac"));
        }

        #endregion
    }
}