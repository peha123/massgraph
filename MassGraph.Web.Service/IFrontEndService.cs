﻿using System.Collections.Generic;
using System.ServiceModel;

using MassGraph.Business.Common.Dto;

namespace MassGraph.Web.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFrontEndService" in both code and config file together.
    [ServiceContract]
    public interface IFrontEndService
    {
        #region Public Methods

        [OperationContract]
        IList<NodeDto> GetAllNodes();

        #endregion
    }
}