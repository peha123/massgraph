﻿using System.Collections.Generic;
using System.Linq;

using MassGraph.Business.Common.Dto;
using MassGraph.Business.Common.Interfaces;

namespace MassGraph.Web.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FrontEndService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FrontEndService.svc or FrontEndService.svc.cs at the Solution Explorer and start debugging.
    public class FrontEndService : IFrontEndService
    {
        #region Properties

        public IGraphEngine GraphEngine { private get; set; }

        #endregion

        #region IFrontEndService Members

        public IList<NodeDto> GetAllNodes()
        {
            return GraphEngine.GetUndirectedGraph().Values.ToList();
        }

        #endregion
    }
}