﻿using System;

using Autofac;
using Autofac.Integration.Wcf;

using Bootstrap;
using Bootstrap.Autofac;

namespace MassGraph.Web.Service
{
    public class Global : System.Web.HttpApplication
    {
        #region Protected and Private Methods

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.With.Autofac().UsingAutoRegistration().Start();
            AutofacHostFactory.Container = (ILifetimeScope)Bootstrapper.ContainerExtension.Container;
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        #endregion
    }
}