﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using EntityFramework.Extensions;

using MassGraph.Business.Common.Dto;
using MassGraph.Business.Common.Interfaces;
using MassGraph.DAL;

using QuickGraph;
using QuickGraph.Algorithms;

namespace MassGraph.Business
{
    public class GraphEngine : IGraphEngine
    {
        #region IGraphEngine Members

        public IList<NodeDto> CalculateShortestPath(int node1Id, int node2Id)
        {
            // retrieve edges
            SEdge<int>[] edges;
            using (GraphContext context = new GraphContext())
            {
                IQueryable<Edge> dbEdges = context.Edges.Where(e => e.Node1Id != null && e.Node2Id != null);
                edges = dbEdges.ToList().Select(e => new SEdge<int>(e.Node1Id.Value, e.Node2Id.Value)).ToArray();
            }

            // calculage shortest path using IDs
            UndirectedGraph<int, SEdge<int>> graph = edges.ToUndirectedGraph<int, SEdge<int>>(false);
            TryFunc<int, IEnumerable<SEdge<int>>> tryGetPath = graph.ShortestPathsDijkstra((i) => 1, node1Id);
            IEnumerable<SEdge<int>> path;
            List<NodeDto> nodePath = new List<NodeDto>();
            if (tryGetPath(node2Id, out path))
            {
                IDictionary<int, NodeDto> nodeDict = this.GetUndirectedGraph();

                foreach (SEdge<int> edge in path)
                {
                    nodePath.Add(nodeDict[edge.Source]);
                }
                nodePath.Add(nodeDict[path.Last().Target]);
            }

            return nodePath;
        }

        public IDictionary<int, NodeDto> GetUndirectedGraph()
        {
            using (GraphContext context = new GraphContext())
            {
                // read all nodes
                List<Node> dbNodes = context.Nodes.Include(n => n.Edges).AsNoTracking().ToList();
                Dictionary<int, NodeDto> nodeDict = dbNodes.ToDictionary(n => n.NodeId, n => new NodeDto { NodeId = n.NodeId, Name = n.Name, Adjacents = new List<NodeDto>() });

                // reconstruct undirected edges
                foreach (Node dbNode in dbNodes)
                {
                    NodeDto firstNode = nodeDict[dbNode.NodeId];
                    foreach (Edge edge in dbNode.Edges.Where(e => e.Node1Id != null && e.Node2Id != null))
                    {
                        NodeDto secondNode = nodeDict[edge.Node2Id.Value];
                        if (!firstNode.Adjacents.Contains(secondNode))
                        {
                            firstNode.Adjacents.Add(secondNode);
                        }
                        if (!secondNode.Adjacents.Contains(firstNode))
                        {
                            secondNode.Adjacents.Add(firstNode);
                        }
                    }
                }

                Dictionary<int, NodeDto> nodes = nodeDict.Values.ToDictionary(n => n.NodeId, n => n);
                return nodes;
            }
        }

        /// <summary>
        /// Import all graph nodes.
        /// </summary>
        /// <param name="nodes"></param>
        public void ImportAllNodes(IList<NodeDto> nodes)
        {
            // delete old data
            this.WipeAllData();

            // import new data
            using (GraphContext context = new GraphContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false; // speed optimalization

                // save nodes, construct edges
                foreach (NodeDto nodeDto in nodes)
                {
                    Node node = new Node { NodeId = nodeDto.NodeId, Name = nodeDto.Name };
                    context.Nodes.Add(node);
                    foreach (NodeDto adjacentDto in nodeDto.Adjacents)
                    {
                        Edge edge = new Edge { Node1Id = node.NodeId, Node2Id = adjacentDto.NodeId };
                        context.Edges.Add(edge);
                    }
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Empty all graph data from the database.
        /// </summary>
        public void WipeAllData()
        {
            using (GraphContext context = new GraphContext())
            {
                context.Edges.Delete();
                context.Nodes.Delete();
            }
        }

        #endregion
    }
}