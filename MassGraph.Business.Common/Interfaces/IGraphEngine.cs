﻿using System.Collections.Generic;

using MassGraph.Business.Common.Dto;

namespace MassGraph.Business.Common.Interfaces
{
    public interface IGraphEngine
    {
        #region Public Methods

        IList<NodeDto> CalculateShortestPath(int node1Id, int node2Id);

        IDictionary<int, NodeDto> GetUndirectedGraph();

        void ImportAllNodes(IList<NodeDto> nodes);

        void WipeAllData();

        #endregion
    }
}