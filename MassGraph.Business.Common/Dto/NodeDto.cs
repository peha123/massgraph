﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace MassGraph.Business.Common.Dto
{
    [Serializable]
    [XmlRoot("node")]
    public class NodeDto
    {
        #region Properties

        [XmlIgnore]
        public List<NodeDto> Adjacents { get; set; }

        [XmlElement("label")]
        public string Name { get; set; }

        [XmlElement("id")]
        public int NodeId { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("{0}({1})->{2}", Name, NodeId, string.Join(", ", Adjacents.Select(a => string.Format("{0}({1})", a.Name, a.NodeId))));
        }

        #endregion
    }
}