﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

using MassGraph.Business.Common.Dto;

namespace MassGraph.Console
{
    internal class Program
    {
        #region Protected and Private Methods

        private static void ImportAllFiles()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(NodeDto));
            List<NodeDto> nodes = new List<NodeDto>();

            // define XSD
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add("", XmlReader.Create(new StringReader(Resources.NodeXsd)));

            // read and parse all files
            string[] filePaths = Directory.GetFiles(@".\InputFiles", "*.xml");
            foreach (string filePath in filePaths)
            {
                // XSD validate
                XDocument nodeXml = XDocument.Load(filePath);
                nodeXml.Validate(schemas, (o, e) => { throw new FormatException(string.Format("Error validating file '{0}': {1}", filePath, e.Message)); });

                // deserialize node
                NodeDto node = (NodeDto)xmlSerializer.Deserialize(new StringReader(nodeXml.ToString()));
                node.Adjacents = nodeXml.Element("node").Element("adjacentNodes").Elements("id").Select(a => new NodeDto { NodeId = int.Parse(a.Value) }).ToList();
                nodes.Add(node);
            }

            // call service to import
            using (DataManagementService.DataManagementServiceClient service = new DataManagementService.DataManagementServiceClient())
            {
                service.ImportAllNodes(nodes.ToArray());
            }
        }

        private static void Main(string[] args)
        {
            ImportAllFiles();
        }

        #endregion
    }
}