﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MassGraph.Web.Startup))]
namespace MassGraph.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
