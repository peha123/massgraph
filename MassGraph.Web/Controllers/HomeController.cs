﻿using System.Web.Mvc;

using MassGraph.Business.Common.Dto;
using MassGraph.Web.FrontEndService;

namespace MassGraph.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Public Methods

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Index()
        {
            using (FrontEndServiceClient client = new FrontEndServiceClient())
            {
                NodeDto[] nodes = client.GetAllNodes();
            }
            return View();
        }

        #endregion
    }
}